import unittest
from synacor import Syncanor

class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.syncanor = Syncanor()
        self.syncanor.memory = [0] * Syncanor.upper_literal_boundry

    def test_nop_lower_to_upper(self):
        syncanor = self.syncanor
        syncanor.not_op(syncanor.lower_register_index, syncanor.lower_literal_boundry)
        self.assertEqual(syncanor.registers[syncanor.lower_register_index], syncanor.upper_literal_boundry)

    def test_nop_upper_to_lower(self):
        syncanor = self.syncanor
        syncanor.not_op(syncanor.lower_register_index, syncanor.upper_literal_boundry)
        self.assertEqual(syncanor.registers[syncanor.lower_register_index], syncanor.lower_literal_boundry)
    
    def test_nop_special(self):
        syncanor = self.syncanor
        test_val = 21845
        expected = 10922
        syncanor.not_op(syncanor.lower_register_index, test_val)
        self.assertEqual(syncanor.registers[syncanor.lower_register_index], expected)
        
    def test_rmem_memory_to_register(self):
        syncanor = self.syncanor
        
        test_val = 21845
        test_memory_idx = 0
        syncanor.memory[0] = test_val

        syncanor.rmem(syncanor.lower_register_index, test_memory_idx)
        self.assertEqual(syncanor.registers[syncanor.lower_register_index], test_val)

    def test_rmem_register_to_register(self):
        syncanor = self.syncanor
        
        test_val = 21845
        syncanor.memory[0] = test_val
        syncanor.registers[syncanor.lower_register_index] = 0

        syncanor.rmem(syncanor.upper_register_index, syncanor.lower_register_index)
        self.assertEqual(syncanor.registers[syncanor.upper_register_index], test_val)

    def test_wmem_value(self):
        syncanor = self.syncanor
        
        test_val = 21845
        syncanor.registers[syncanor.lower_register_index] = 0

        syncanor.wmem(syncanor.lower_register_index, test_val)
        self.assertEqual(syncanor.memory[0], test_val)

    def test_wmem_register(self):
        syncanor = self.syncanor
        
        test_val = 21845
        syncanor.registers[syncanor.lower_register_index] = test_val
        syncanor.registers[syncanor.upper_register_index] = 100
        
        syncanor.wmem(syncanor.upper_register_index, syncanor.lower_register_index)
        
        self.assertEqual(syncanor.memory[100], test_val)

    def test_addition(self):
        syncanor = self.syncanor
        syncanor.memory = [9,32768,32769,4]
        syncanor.execute()
        self.assertEqual(syncanor.registers[32768], 4)

    def test_addition_overflow(self):
        syncanor = self.syncanor
        syncanor.add(syncanor.lower_register_index, 32758, 15)
        self.assertEqual(syncanor.registers[syncanor.lower_register_index], 5)

if __name__ == '__main__':
    unittest.main()
